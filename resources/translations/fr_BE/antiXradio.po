# antiXradio
# Copyright (C) 2023 antiX Community
# This file is distributed under the same license as the antixradio package (GPL v.3)
# Wallon, 2023.
# 
# Translators:
# Wallon Wallon, 2023
# 
msgid ""
msgstr ""
"Project-Id-Version: version 0.45\n"
"Report-Msgid-Bugs-To: https://www.antixforum.com\n"
"POT-Creation-Date: 2023-12-30 08:42+0100\n"
"PO-Revision-Date: 2023-12-24 11:50+0000\n"
"Last-Translator: Wallon Wallon, 2023\n"
"Language-Team: French (Belgium) (https://app.transifex.com/anticapitalista/teams/10162/fr_BE/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: fr_BE\n"
"Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;\n"

#. Text displayed in a button in main window.
#: antiXradio:55
msgid "Exit"
msgstr "Quitter"

#. Text displayed in a button in main window.
#: antiXradio:56
msgid "Play"
msgstr "Écouter"

#. Text displayed in a button in main window.
#: antiXradio:57
msgid "Stop"
msgstr "Arrêter"

#. Text displayed in a button in main window.
#: antiXradio:58
msgid "Load another list"
msgstr "Charger une autre liste"

#. Text displayed in a button in main dialog and info dialog both.
#: antiXradio:59
msgid "Turn off the radio"
msgstr "Éteindre la radio"

#. Text displayed in a button in info dialog that appears after clicking the X
#. in upper Window border of main window
#: antiXradio:60
msgid "Return"
msgstr "Retour"

#. Header line of stations list displayed in main window.
#: antiXradio:61
msgid "Radio stations:"
msgstr "Stations radio:"

#. Window title supplement in main window's upper border, present if reception
#. was stopped.
#: antiXradio:62
msgid "(Standby)"
msgstr "(En attente)"

#. Text displayed in a pulldown in file selection dialog for all files with
#. the ".txt" extension (no other choices)
#: antiXradio:63
msgid "Text files"
msgstr "Fichiers texte"

#. Text of file selection dialog for selecting another stations list.
#: antiXradio:64
msgid "Select the radio list you want to load:"
msgstr "Sélectionnez la liste de radios que vous souhaitez charger:"

#. Text displayed in a button in main dialog and info dialog both.
#: antiXradio:65
msgid "Close window"
msgstr "Fermer la fenêtre"

#. Window title supplement in main window's upper border, present if reception
#. was stopped.
#: antiXradio:66
msgid "stopped"
msgstr "arrêté"

#. Text displayed in a button in main window.
#: antiXradio:67
msgid "Record"
msgstr "Enregistrer"

#. Tooltip for main window's exit button.
#. Never use a default exclamation mark in this text. If you need it, use the
#. typographical replacement, e.g. ctrl+shift+u FF01 (Unicode full width
#. exclamation mark)
#: antiXradio:68
msgid ""
"This will power off the radio and stop any running station or record. If you"
" want just to close this window instead, keeping the radio playing, use the "
"X in upper window border or the ESC key on your keyboard instead. To stop "
"reception later call antiXradio again, and press this button then."
msgstr ""
"Cela éteindra la radio et arrêtera toute station ou enregistrement en cours."
" Si vous voulez simplement fermer cette fenêtre, tout en gardant la radio en"
" marche, utilisez le X dans le bord supérieur de la fenêtre ou la touche ESC"
" de votre clavier à la place. Pour arrêter la réception plus tard, appelez à"
" nouveau antiXradio et appuyez alors sur ce bouton."

#. Tooltip for main window's change station button.
#. Never use a default exclamation mark in this text. If you need it, use the
#. typographical replacement, e.g. ctrl+shift+u FF01 (Unicode full width
#. exclamation mark)
#: antiXradio:71
msgid ""
"This will allow you to select another stations list from a folder on your "
"file system. You may edit the stations lists within a text editor, just make"
" sure to keep the format you find in it."
msgstr ""
"Cela vous permettra de sélectionner une autre liste de stations dans un "
"dossier de votre système de fichiers. Vous pouvez modifier les listes de "
"stations à l’aide d’un éditeur de texte, mais veillez à conserver le format "
"que vous y trouverez."

#. Tooltip for main window's stop button.
#. Never use a default exclamation mark in this text. If you need it, use the
#. typographical replacement, e.g. ctrl+shift+u FF01 (Unicode full width
#. exclamation mark)
#: antiXradio:73
msgid ""
"Stop current radio reception or recording. Starting another station or "
"another recording will also stop the current reception."
msgstr ""
"Arrêter la réception radio ou l’enregistrement en cours. Le démarrage d’une "
"autre station ou d’un autre enregistrement interrompra également la "
"réception en cours."

#. Tooltip for main window's recording button.
#. Never use a default exclamation mark in this text. If you need it, use the
#. typographical replacement, e.g. ctrl+shift+u FF01 (Unicode full width
#. exclamation mark)
#: antiXradio:75
msgid ""
"Record what the selected station plays to a file in your home folder's "
"“Radio-Recordings” directory. The file will have .ts extension and can be "
"played with MPV media player. You may convert it after recording was "
"completed to whatever format (mp3, mka, ogg m4a) by means of a audio "
"conversion tool (e.g. ffmpeg or audacity)"
msgstr ""
"Enregistrer ce que la station sélectionnée diffuse dans un fichier qui se "
"trouvera dans le répertoire « Radio-Enregistrements » de votre dossier home."
" Le fichier portera l’extension .ts et pourra être lu avec le lecteur MPV. "
"Une fois l’enregistrement terminé, vous pouvez le convertir dans n’importe "
"quel format (mp3, mka, ogg m4a) à l’aide d’un outil de conversion audio (par"
" exemple, ffmpeg ou audacity)."

#. Tooltip for main window's start reception button.
#. Never use a default exclamation mark in this text. If you need it, use the
#. typographical replacement, e.g. ctrl+shift+u FF01 (Unicode full width
#. exclamation mark)
#: antiXradio:79
msgid ""
"This will start reception of selected radio station. You need to be "
"connected to the internet to receive the radio streams via network."
msgstr ""
"Cela lancera la réception de la station de radio sélectionnée. Vous devez "
"être connecté à Internet pour recevoir les flux radio via le réseau."

#. Button-tooltip in stations list update warning dialog at startup, right
#. button.
#. Never use a default exclamation mark in this text. If you need it, use the
#. typographical replacement, e.g. ctrl+shift+u FF01 (Unicode full width
#. exclamation mark)
#. Please don't translate the path ~/.config/antiXradio/ and also not the file
#. extension .bak.
#: antiXradio:81
msgid ""
"This will bluntly overwrite all stations lists in "
"~/.config/antiXradio/stations (all the old files will be saved to .bak "
"extension. Existing .bak files will get overwriten)"
msgstr ""
"Cela écrasera purement et simplement toutes les listes de stations dans "
"~/.config/antiXradio/stations (tous les anciens fichiers seront sauvegardés "
"avec l’extension .bak. Les fichiers .bak existants seront écrasés)."

#. Button-tooltip in stations list update warning dialog at startup, middle
#. button.
#. Never use a default exclamation mark in this text. If you need it, use the
#. typographical replacement, e.g. ctrl+shift+u FF01 (Unicode full width
#. exclamation mark)
#: antiXradio:83
msgid ""
"Stations lists you have modified will not be replaced, instead the update "
"will be written next to them with the extension .new so you can compare "
"using e.g. »meld«, and transfer from the update to your individual lists "
"what you feel like."
msgstr ""
"Les listes de stations que vous avez modifiées ne seront pas remplacées. La "
"mise à jour sera écrite à côté d’elles avec l’extension .new afin que vous "
"puissiez la comparer en utilisant par ex. « meld ». Vous pourrez par la "
"suite, transférer les nouvelles stations radio à partir de la mise à jour, "
"comme bon vous semble, vers vos listes individuelles."

#. Button-tooltip in stations list update warning dialog at startup, left
#. button.
#. Never use a default exclamation mark in this text. If you need it, use the
#. typographical replacement, e.g. ctrl+shift+u FF01 (Unicode full width
#. exclamation mark)
#. Please don't translate the paths ~/.config/antiXradio/stations and
#. /usr/local/lib/antiXradio/stations
#: antiXradio:86
msgid ""
"Please care yourself for updating your collection of lists residing in "
"~/.config/antiXradio/stations from the new files present in "
"/usr/local/lib/antiXradio/stations"
msgstr ""
"Veuillez prendre soin de mettre à jour votre ensemble de listes se trouvant "
"dans ~/.config/antiXradio/stations à partir des nouveaux fichiers présents "
"dans /usr/local/lib/antiXradio/stations."

#. Tooltip for left button in info dialog that appears after clicking the X in
#. upper Window border of main window
#. Never use a default exclamation mark in this text. If you need it, use the
#. typographical replacement, e.g. ctrl+shift+u FF01 (Unicode full width
#. exclamation mark)
#: antiXradio:88
msgid "Stop radio broadcasts and recordings if any, exit antiXradio"
msgstr ""
"Arrêter les diffusions et les enregistrements radio s’il y en a, quitter "
"antiXradio"

#. Tooltip for middle button in info dialog that appears after clicking the X
#. in upper Window border of main window
#. Never use a default exclamation mark in this text. If you need it, use the
#. typographical replacement, e.g. ctrl+shift+u FF01 (Unicode full width
#. exclamation mark)
#: antiXradio:89
msgid "Cancel action and return to the main window"
msgstr "Annuler l’action et revenir à la fenêtre principale."

#. Tooltip for right butto in info dialog that appears after clicking the X in
#. upper Window border of main window
#. Never use a default exclamation mark in this text. If you need it, use the
#. typographical replacement, e.g. ctrl+shift+u FF01 (Unicode full width
#. exclamation mark)
#: antiXradio:90
msgid ""
"Listen to the radio station without distracting window. Launch the radio "
"later again from antiX main menu to change radio channel, stop radio "
"reception or recording, or to exit antiXradio."
msgstr ""
"Écouter la station radio sans fenêtre distrayante. Lancez à nouveau la radio"
" plus tard à partir du menu principal d’antiX pour; changer de station "
"radio, arrêter la réception d’une radio, arrêter un enregistrement ou "
"quitter antiXradio."

#. Base folder name for storing recordings (sub-folder of user's home folder)
#: antiXradio:108
msgid "Radio-Recordings"
msgstr "Radio-Enregistrements"

#. One or more software packages were not found. The name of the package
#. missing preceeds this.
#. Command line error message text
#: antiXradio:129
msgid "not found."
msgstr "non trouvé(s)."

#. Do not translate the "\n" code. These are line breaks. If your text is too
#. long, you can add "\n" codes.
#. Command line error text
#: antiXradio:131
msgid ""
"The installation of this script has failed. Make sure the above "
"command(s)\\nis (are) available on your system and please contact package "
"maintainer.\\nantiXradio Leaving."
msgstr ""
"L’installation de ce script a échoué. Assurez-vous que la (les) "
"commande(s)\\nci-dessus est (sont) disponible(s) sur votre système et "
"contactez le\\nresponsable du paquet.\\nQuitter antiXradio."

#. translatable window header, used in main and exit info window both. I gues
#. only the part "radio" can get translated, since antiX is a name.
#: antiXradio:199 antiXradio:216 antiXradio:224 antiXradio:297 antiXradio:369
#: antiXradio:377 antiXradio:467 antiXradio:468 antiXradio:469 antiXradio:470
#: antiXradio:617 antiXradio:635
msgid "antiXradio"
msgstr "antiXradio"

#. Text in info dialog that appears after clicking the X in upper Window
#. border of main window
#. Do not translate the codes "\n\t" or "\n". These are line breaks. If your
#. text is too long, you can add codes "\n\t" or "\n".
#. Never translate "$exit_button_text". Also <b> and </b> is not translatable,
#. these are tags for bold text in between. The order
#. of opening <b> and closing tags </b> MUST be observed properly.
#: antiXradio:200
#, sh-format
msgid ""
"\\n\\t<b> Please note! </b>\\n\\n\\t When using the ESC key or the X from "
"upper window\\n\\t borders, only the window will be closed while the\\n\\t "
"radio keeps playing (and/or recording).\\n\\t To power off the radio later, "
"please start antiX Radio \\n\\t and click the '<b>$exit_button_text</b>' "
"button on the lower \\n\\t left section of it's window. \\n\\n"
msgstr ""
"\\n\\t<b> Attention! </b>\\n\\n\\t Lorsque vous utilisez la touche ESC ou le"
" X des\\n\\t bords supérieurs de la fenêtre, seule la fenêtre\\n\\t sera "
"fermée pendant que la radio continue de diffuser\\n\\t (et/ou "
"d’enregistrer).\\n\\t Pour éteindre la radio plus tard, veuillez "
"démarrer\\n\\t antiX Radio et cliquer sur le bouton « "
"<b>$exit_button_text</b> » \\n\\t dans la partie inférieure gauche de la "
"fenêtre. \\n\\n"

#. Checkbox text in info dialog that appears after clicking the X in upper
#. Window border of main window
#: antiXradio:201
msgid "Got it. Don’t show me again."
msgstr "J’ai compris. Ne me le montrez plus."

#. Headertext in info dialog (visible when not connection to internet was
#. found on startup)
#: antiXradio:242
msgid "Internet connection error."
msgstr "Erreur de connexion Internet."

#. Text in info dialog (visible when not connection to internet was found on
#. startup)
#: antiXradio:243
msgid "No Internet connection found."
msgstr "Aucune connexion Internet trouvée."

#. Do not translate the "\n" code. These are line breaks. If your text is
#. too long, you can add "\n" codes.
#. Text in info dialog (visible when not connection to internet was found on
#. startup)
#: antiXradio:243
msgid ""
"Please connect to the Internet before\\ntrying again or you may exit "
"antiXradio."
msgstr ""
"Veuillez vous connecter à Internet avant de\\nréessayer ou vous pouvez "
"quitter antiXradio."

#. Text displayed in a button in internet connection error dialog.
#: antiXradio:244
msgid "Exit antiXradio"
msgstr "Quitter antiXradio"

#. Text displayed in a button in internet connection error dialog.
#: antiXradio:244
msgid "Try again"
msgstr "Essayez à nouveau"

#. Base Filename for recording, followed by a date in locale format. Please
#. make sure never to use a slash "/" in your translation of this string.
#: antiXradio:333
msgid "radio-recording"
msgstr "radio-enregistrement"

#. Title of yad notifiction in system's status bar.
#: antiXradio:351
msgid "antiXradio recording"
msgstr "antiXradio enregistrement"

#. Tooltip of notification icon in system's status bar, to be translated to
#. the meaning "There is a recording running".
#. Never use a default exclamation mark in this text. If you need it, use the
#. typographical replacement, e.g. ctrl+shift+u FF01 (Unicode full width
#. exclamation mark)
#: antiXradio:352 antiXradio:357
msgid "Running antiXradio recording"
msgstr "antiXradio est en train de réaliser un enregistrement."

#. Stations list update warning dialog at startup, window border text.
#: antiXradio:483
msgid "antiXradio stations list update"
msgstr "Mise à jour de la liste des stations antiXradio."

#. Stations list update warning dialog at startup, header text in bold.
#: antiXradio:484
msgid "antiXradio's stations lists have been actualised."
msgstr "Les listes des stations d’antiXradio ont été actualisées."

#. Stations list update warning dialog at startup, dialog text.
#. Please make sure to place all the \n line breaks in proper position for
#. your
#. language to make your text fit in the window neatly. You can use more of \n
#. line breaks if needed, also line length is to a certain degree variable.
#. Don't translate \\t/usr/local/lib/antiXradio/stations\n and
#. \\t~/.config/antiXradio/stations\n paths.
#: antiXradio:485
msgid ""
"The new lists are waiting in\n"
"\\t/usr/local/lib/antiXradio/stations\n"
"directory to be copied to your personal collection\n"
"of stations lists in your home folder\n"
"\\t~/.config/antiXradio/stations\n"
"From the buttons’ tooltips you can learn the options\n"
"you have. Do you want antiXradio to do the update of\n"
"your collection for you?"
msgstr ""
"Les nouvelles listes attendent dans le répertoire\n"
"\\t/usr/local/lib/antiXradio/stations\n"
"d’être copiées dans votre collection personnelle\n"
"de listes de stations dans votre dossier home\n"
"\\t~/.config/antiXradio/stations.\n"
"Les infobulles des boutons vous permettent de\n"
"connaître les options dont vous disposez.\n"
"Voulez-vous qu’antiXradio fasse la mise à jour de\n"
"votre collection pour vous?"

#. Button engraving in stations list update warning dialog at startup
#: antiXradio:493
msgid "No thanks"
msgstr "Non merci"

#. Button engraving in stations list update warning dialog at startup
#: antiXradio:494
msgid "Do the math for me"
msgstr "Faire le calcul pour moi"

#. Button engraving in stations list update warning dialog at startup
#: antiXradio:495
msgid "Just overwrite"
msgstr "Écraser directement"

#. Checkbox label in main window
#: antiXradio:632
msgid ""
"Displays a small window with information on the current radio programme and "
"the station."
msgstr ""
"Affiche une petite fenêtre avec des informations sur le programme radio en "
"cours et la station."

#. Window title supplement in main window's upper border, present if a
#. recording is running. Please keep the double exclamation marks for better
#. perceptibility (or use another eyecatcher of your choice)
#: antiXradio:668
msgid "!! Recording !!"
msgstr "!! Enregistrement !!"
