# SR Sveriges Radio. Quelle: https://sverigesradio.se/artikel/mp3-lankar-som-m3u-192-kbps
# Details about SR broadcasting station see https://sv.wikipedia.org/wiki/Sveriges_Radio
# last updated 12/2023 (Publicerat onsdag 9 juni 2010 kl 15.02)
SR P1,https://sverigesradio.se/topsy/direkt/132-hi-mp3.m3u
SR P2,https://sverigesradio.se/topsy/direkt/2562-hi-mp3.m3u
SR P2 (HQ),https://http-live.sr.se/p2musik-aac-320
SR P3,https://sverigesradio.se/topsy/direkt/164-hi-mp3.m3u
SR P4+,https://sverigesradio.se/topsy/direkt/4951-hi-mp3.m3u
SR Ekot sänder direkt,https://sverigesradio.se/topsy/direkt/4540-hi-mp3.m3u
SR P4 Blekinge,https://sverigesradio.se/topsy/direkt/213-hi-mp3.m3u
SR P4 Dalarna,https://sverigesradio.se/topsy/direkt/223-hi-mp3.m3u
SR P4 Gotland,https://sverigesradio.se/topsy/direkt/205-hi-mp3.m3u
SR P4 Gävleborg,https://sverigesradio.se/topsy/direkt/210-hi-mp3.m3u
SR P4 Göteborg,https://sverigesradio.se/topsy/direkt/212-hi-mp3.m3u
SR P4 Halland,https://sverigesradio.se/topsy/direkt/220-hi-mp3.m3u
SR P4 Jämtland,https://sverigesradio.se/topsy/direkt/200-hi-mp3.m3u
SR P4 Jönköping,https://sverigesradio.se/topsy/direkt/203-hi-mp3.m3u
SR P4 Kalmar,https://sverigesradio.se/topsy/direkt/201-hi-mp3.m3u
SR P4 Kristianstad,https://sverigesradio.se/topsy/direkt/211-hi-mp3.m3u
SR P4 Kronoberg,https://sverigesradio.se/topsy/direkt/214-hi-mp3.m3u
SR P4 Malmöhus,https://sverigesradio.se/topsy/direkt/207-hi-mp3.m3u
SR P4 Norrbotten,https://sverigesradio.se/topsy/direkt/209-hi-mp3.m3u
SR P4 Sjuhärad,https://sverigesradio.se/topsy/direkt/206-hi-mp3.m3u
SR P4 Skaraborg,https://sverigesradio.se/topsy/direkt/208-hi-mp3.m3u
SR P4 Stockholm,https://sverigesradio.se/topsy/direkt/701-hi-mp3.m3u
SR P4 Södertälje,https://sverigesradio.se/topsy/direkt/5283-hi-mp3.m3u
SR P4 Sörmland,https://sverigesradio.se/topsy/direkt/202-hi-mp3.m3u
SR P4 Uppland,https://sverigesradio.se/topsy/direkt/218-hi-mp3.m3u
SR P4 Värmland,https://sverigesradio.se/topsy/direkt/204-hi-mp3.m3u
SR P4 Väst,https://sverigesradio.se/topsy/direkt/219-hi-mp3.m3u
SR P4 Västerbotten,https://sverigesradio.se/topsy/direkt/215-hi-mp3.m3u
SR P4 Västernorrland,https://sverigesradio.se/topsy/direkt/216-hi-mp3.m3u
SR P4 Västmanland,https://sverigesradio.se/topsy/direkt/217-hi-mp3.m3u
SR P4 Örebro,https://sverigesradio.se/topsy/direkt/221-hi-mp3.m3u
SR P4 Östergötland,https://sverigesradio.se/topsy/direkt/222-hi-mp3.m3u
SR P3 Din gata,https://sverigesradio.se/topsy/direkt/2576-hi-mp3.m3u
SR P2 Språk och musik,https://sverigesradio.se/topsy/direkt/163-hi-mp3.m3u
SR P6,https://sverigesradio.se/topsy/direkt/166-hi-mp3.m3u
SR Radioapans knattekanal,https://sverigesradio.se/topsy/direkt/2755-hi-mp3.m3u
SR Sápmi,https://sverigesradio.se/topsy/direkt/224-hi-mp3.m3u
SR Sveriges Radio Finska,https://sverigesradio.se/topsy/direkt/226-hi-mp3.m3u
SR Extra 01,https://sverigesradio.se/topsy/direkt/2383-hi-mp3.m3u
SR Extra 02,https://sverigesradio.se/topsy/direkt/2384-hi-mp3.m3u
SR Extra 03,https://sverigesradio.se/topsy/direkt/2385-hi-mp3.m3u
SR Extra 04,https://sverigesradio.se/topsy/direkt/2386-hi-mp3.m3u
SR Extra 05,https://sverigesradio.se/topsy/direkt/2387-hi-mp3.m3u
SR Extra 06,https://sverigesradio.se/topsy/direkt/2388-hi-mp3.m3u
SR Extra 07,https://sverigesradio.se/topsy/direkt/2389-hi-mp3.m3u
SR Extra 08,https://sverigesradio.se/topsy/direkt/2390-hi-mp3.m3u
SR Extra 09,https://sverigesradio.se/topsy/direkt/3268-hi-mp3.m3u
SR Extra 10,https://sverigesradio.se/topsy/direkt/3269-hi-mp3.m3u

