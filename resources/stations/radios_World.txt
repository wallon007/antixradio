# These are either multilingual radio stations from countries all over the world, transferring in different time frames different languages or
# provide an English language programme discussing topics of local or international interest from the perspective of the respective country the
# station resides in. Alphabetical sort order.

Czechia: Radio Prague (ĈRo International),https://rozhlas.stream/radio_prague_int_high.aac # source: https://portal.rozhlas.cz/stanice

Esthonia: Raadio Tallinn (ERR International),https://icecast.err.ee/raadiotallinnkorge.mp3.m3u # 320kbps mp3 stream; source: https://icecast.err.ee/
# alternatively: https://icecast.err.ee/raadiotallinn.opus.m3u # 128kbps ogg opus stream

Germany: Deutsche Welle (DW International),https://dwamdstream107.akamaized.net/hls/live/2017968/dwstream107/stream01/streamPlaylist.m3u8 # source: https://www.dw.com

Great Britain: BBC_World_News,http://as-hls-ww-live.akamaized.net/pool_904/live/ww/bbc_world_service/bbc_world_service.isml/bbc_world_service-audio%3d48000.norewind.m3u8

Ukraine: Всесвітня служба радіо (Radio Ukraine International),http://radio.ukr.radio:8000/ur4-mp3 # 192kbps mp3 sream. source: http://www.nrcu.gov.ua/maps

United States: Voice of America (VOA Global),https://www.voanews.com/live/audio/60

UN News: United Nations Radio,https://www.youtube.com/watch?v=wfAa1GiNdgM



#######################################################
# Please add the world radio international service of #
# your country and send it back to www.antiXforum.com #
# So other people can listen to it.      Many thanks! #
#######################################################
