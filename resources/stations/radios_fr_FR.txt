# Reference website: https://toutes-les-radios.fr
# Updated on 18/12/2023, all feeds functional
#
France Bleu,http://icecast.radiofrance.fr/fb1071-midfi.mp3
France Culture,http://direct.franceculture.fr/live/franceculture-midfi.mp3
France Info,http://direct.franceinfo.fr/live/franceinfo-midfi.mp3
France Inter,http://direct.franceinter.fr/live/franceinter-midfi.mp3
France Musique,http://direct.francemusique.fr/live/francemusique-midfi.mp3
Alternative Radio,https://listen.radioking.com/radio/182263/stream/224140
Chante France,https://chantefrance75.ice.infomaniak.ch/chantefrance75-96.aac
Classic Rock,https://classicrock.streeemer.com/listen/classic_rock/radio.aac
Fip,https://stream.radiofrance.fr/fip/fip_hifi.m3u8?id=progradio
Fun Radio,https://streamer-04.rtl.fr/fun-1-44-128?id=webFUNRADIO
Hot Mix Radio 80,https://streamingp.shoutcast.com/hotmixradio-80-128.mp3?
Jazz Radio (jazz and soul),https://jazzblues.ice.infomaniak.ch/jazzblues-high.aac?i=69920
La Grosse Radio (Reggae),https://hd.lagrosseradio.info/lagrosseradio-reggae-192.mp3
La Radio Sympa,https://radio11.pro-fhi.net:19095/stream
Latina,https://start-latina.ice.infomaniak.ch/start-latina-high
Mouv’,http://icecast.radiofrance.fr/mouv-midfi.mp3
Nostalgie,http://scdn.nrjaudio.fm/adwz2/fr/30601/mp3_128.mp3?origine=fluxradios
NRJ,http://cdn.nrjaudio.fm/adwz2/fr/30001/mp3_128.mp3?origine=fluxradios
Radio Classique,http://radioclassique.ice.infomaniak.ch/radioclassique-high.mp3
Radio Meuh,https://radiomeuh.ice.infomaniak.ch:8000/radiomeuh-128.mp3
Radio Paname,http://paname.ice.infomaniak.ch/paname-128.mp3
Radio Sans Soupe,https://listen.radioking.com/radio/25504/stream/58228
RadioTamTam,https://listen.radioking.com/radio/10736/stream/295960
Tropiques FM,https://listen.radioking.com/radio/8916/stream/19088
Vibration,https://vibration.ice.infomaniak.ch/vibration-high.mp3
Voltage,https://start-voltage.ice.infomaniak.ch/start-voltage-high.mp3
Zicline,https://ecmanager.pro-fhi.net/ziclineradio
