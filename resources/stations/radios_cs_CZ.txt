# ČRo Český rozhlas, http://www.rozhlas.cz/
# Deatails about station: https://cs.wikipedia.org/wiki/%C4%8Cesk%C3%BD_rozhlas
# Source: https://portal.rozhlas.cz/stanice (click all pulldowns "Všechny online streamy")
# last updated 12/2023

ČRo 1 Radiožurnál,https://rozhlas.stream/radiozurnal_high.aac # (160 kbps aac stream)
# or alternatively https://rozhlas.stream/radiozurnal_mp3_128.mp3 # (128 kbps mp3 stream)

ČRo 2 Dvojka,https://rozhlas.stream/dvojka_high.aac # (160 kbps aac stream)
# or alternatively https://rozhlas.stream/dvojka_mp3_128.mp3 # (128 kbps mp3 stream)

ČRo 3 Vltava,https://rozhlas.stream/vltava_high.aac # (160 kbps aac stream)
# or alternatively https://rozhlas.stream/vltava_mp3_128.mp3 # (128 kbps mp3 stream)

ČRo Plus,https://rozhlas.stream/plus_high.aac # (160 kbps aac stream)
# or alternatively https://rozhlas.stream/plus_mp3_128.mp3 # (128 kbps mp3 stream)

ČRo Radiožurnál Sport,https://rozhlas.stream/radiozurnal_sport_high.aac # (160 kbps aac stream)
# or alternatively https://rozhlas.stream/sport_mp3_128.mp3 # (128 kbps mp3 stream)

ČRo Radio Wave,https://rozhlas.stream/radio_wave_high.aac # (160 kbps aac stream)
# or alternatively https://rozhlas.stream/wave_mp3_128.mp3 # (128 kbps mp3 stream)

ČRo D-Dur,https://rozhlas.stream/ddur_high.aac # (160 kbps aac stream)
# or alternatively https://rozhlas.stream/ddur_mp3_128.mp3 # (128 kbps mp3 stream)

ČRo Jazz,https://rozhlas.stream/jazz_high.aac # (160 kbps aac stream)
# or alternatively https://rozhlas.stream/jazz_mp3_128.mp3 # (128 kbps mp3 stream)

ČRo Radio Junior,https://rozhlas.stream/radio_junior_high.aac # (160 kbps aac stream)
# or alternatively https://rozhlas.stream/radio_junior.mp3 # (128 kbps mp3 stream)

ČRo Radio Junior+ PÍSNIČKY,https://rozhlas.stream/radio_junior_pisnicky_high.aac # (160 kbps aac stream)
# or alternatively https://rozhlas.stream/radio_junior_pisnicky.mp3 # (128 kbps mp3 stream)

ČRo Pohoda, https://rozhlas.stream/pohoda_high.aac # (160 kbps aac stream)
# or alternatively https://rozhlas.stream/pohoda_mp3_128.mp3 # (128 kbps mp3 stream)

ČRo Rádio DAB Praha,https://rozhlas.stream/radio_dab_praha_high.aac # (160 kbps aac stream)
# or alternatively https://rozhlas.stream/regina_mp3_128.mp3 # (128 kbps mp3 stream)

ČRo Rádio Brno,https://rozhlas.stream/brno_high.aac # (160 kbps aac stream)
# or alternatively https://rozhlas.stream/brno.mp3 # (128 kbps mp3 stream)

ČRo Rádio České Budějovice,https://rozhlas.stream/ceske_budejovice_high.aac # (160 kbps aac stream)
# or alternatively https://rozhlas.stream/ceske_budejovice.mp3 # (128 kbps mp3 stream)

ČRo Rádio Hradec Králové, https://rozhlas.stream/hradec_kralove_high.aac # (160 kbps aac stream)
# or alternatively https://rozhlas.stream/hradec_kralove.mp3 # (128 kbps mp3 stream)

ČRo Rádio Karlovy Vary,https://rozhlas.stream/karlovy_vary_high.aac # (160 kbps aac stream)
# or alternatively https://rozhlas.stream/karlovy_vary.mp3 # (128 kbps mp3 stream)

ČRo Rádio Liberec,https://rozhlas.stream/liberec_high.aac # (160 kbps aac stream)
# or alternatively https://rozhlas.stream/liberec.mp3 # (128 kbps mp3 stream)

ČRo Rádio Olomouc,https://rozhlas.stream/olomouc_high.aac # (160 kbps aac stream)
# or alternatively https://rozhlas.stream/olomouc.mp3 # (128 kbps mp3 stream)

ČRo Rádio Ostrava,https://rozhlas.stream/ostrava_high.aac # (160 kbps aac stream)
# or alternatively https://rozhlas.stream/ostrava.mp3 # (128 kbps mp3 stream)

ČRo Rádio Pardubice,https://rozhlas.stream/pardubice_high.aac # (160 kbps aac stream)
# or alternatively https://rozhlas.stream/pardubice.mp3 # (128 kbps mp3 stream)

ČRo Rádio Plzeň,https://rozhlas.stream/plzen_high.aac # (160 kbps aac stream)
# or alternatively https://rozhlas.stream/plzen.mp3 # (128 kbps mp3 stream)

ČRo Rádio Region,https://rozhlas.stream/region_high.aac # (160 kbps aac stream)
# or alternatively https://rozhlas.stream/region.mp3 # (128 kbps mp3 stream)

ČRo Rádio Sever,https://rozhlas.stream/sever_high.aac # (160 kbps aac stream)
# or alternatively https://rozhlas.stream/sever.mp3 # (128 kbps mp3 stream)

ČRo Rádio Vysočina,https://rozhlas.stream/vysocina_high.aac # (160 kbps aac stream)
# or alternatively https://rozhlas.stream/vysocina.mp3 # (128 kbps mp3 stream)

ČRo Rádio Zlín,https://rozhlas.stream/zlin_high.aac # (160 kbps aac stream)
# or alternatively https://rozhlas.stream/zlin.mp3 # (128 kbps mp3 stream)
