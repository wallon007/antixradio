
# WDR, Westdeutscher Rundfunk. Quelle: https://www1.wdr.de/unternehmen/der-wdr/dialog/digitalradio/webradio-100.html
# Details zum Sender: https://de.wikipedia.org/wiki/Westdeutscher_Rundfunk_K%C3%B6ln
# last updated 12/2023
WDR 1 Eins Live,https://wdr-1live-diggi.icecastssl.wdr.de/wdr/1live/diggi/mp3/128/stream.mp3
WDR 1 Diggi,https://wdr-1live-diggi.icecastssl.wdr.de/wdr/1live/diggi/mp3/128/stream.mp3
WDR 1 Dancehits in Dauerschleife,https://wdr-1live-dancehits.icecast.wdr.de/wdr/1live/dancehits/mp3/128/stream.mp3
WDR 1 Chillout in Dauerschleife,https://wdr-1live-chillout.icecast.wdr.de/wdr/1live/chillout/mp3/128/stream.mp3
WDR 1 Top Hits in Dauerschleife,https://wdr-1live-tophits.icecast.wdr.de/wdr/1live/tophits/mp3/128/stream.mp3
WDR 1 Plan B,http://wdr-1live-planb.icecast.wdr.de/wdr/1live/planb/mp3/128/stream.mp3
WDR 1 DJ Session,http://wdr-1live-djsession.icecast.wdr.de/wdr/1live/djsession/mp3/128/stream.mp3
WDR 1 Neu für den Sektor,http://wdr-1live-neufuerdensektor.icecast.wdr.de/wdr/1live/neufuerdensektor/mp3/128/stream.mp3
WDR 1 Hip Hop in Dauerschleife,https://wdr-1live-hiphoprnb.icecast.wdr.de/wdr/1live/hiphoprnb/mp3/128/stream.mp3
WDR 1 Spezial,http://wdr-1live-specials.icecast.wdr.de/wdr/1live/specials/mp3/128/stream.mp3
WDR 1 Club,http://www.wdr.de/wdrlive/media/1live_klubbing.m3u
WDR 1 Kassettendeck,http://www.wdr.de/wdrlive/media/1live_kassettendeck.m3u
WDR 1 Mix der Woche in Dauerschleife,https://wdr-1live-mixderwoche.icecast.wdr.de/wdr/1live/mixderwoche/mp3/128/stream.mp3
WDR 1 Fiehe,http://wdr-1live-fiehe.icecast.wdr.de/wdr/1live/fiehe/mp3/128/stream.mp3
WDR 1 Rock in Dauerschleife,https://wdr-1live-rockhits.icecast.wdr.de/wdr/1live/rockhits/mp3/128/stream.mp3
WDR 2 Rheinland,https://wdr-wdr2-rheinland.icecastssl.wdr.de/wdr/wdr2/rheinland/mp3/128/stream.mp3
WDR 2 Ostwestfalen,https://wdr-wdr2-ostwestfalenlippe.icecastssl.wdr.de/wdr/wdr2/ostwestfalenlippe/mp3/128/stream.mp3
WDR 2 Münsterland,https://wdr-wdr2-muensterland.icecastssl.wdr.de/wdr/wdr2/muensterland/mp3/128/stream.mp3
WDR 2 Ruhrgebiet,https://wdr-wdr2-ruhrgebiet.icecastssl.wdr.de/wdr/wdr2/ruhrgebiet/mp3/128/stream.mp3
WDR 2 Rhein und Ruhr,https://wdr-wdr2-rheinruhr.icecastssl.wdr.de/wdr/wdr2/rheinruhr/mp3/128/stream.mp3
WDR 2 Bergisches Land,https://wdr-wdr2-bergischesland.icecastssl.wdr.de/wdr/wdr2/bergischesland/mp3/128/stream.mp3
WDR 2 Südwestfalen,https://wdr-wdr2-suedwestfalen.icecastssl.wdr.de/wdr/wdr2/suedwestfalen/mp3/128/stream.mp3
WDR 2 Region Aachen,https://wdr-wdr2-aachenundregion.icecastssl.wdr.de/wdr/wdr2/aachenundregion/mp3/128/stream.mp3
WDR 3,https://wdr-wdr3-live.icecastssl.wdr.de/wdr/wdr3/live/mp3/128/stream.mp3
WDR 3 (HQ),https://wdr-wdr3-live.icecastssl.wdr.de/wdr/wdr3/live/mp3/256/stream.mp3
WDR 4,https://wdr-wdr4-live.icecastssl.wdr.de/wdr/wdr4/live/mp3/128/stream.mp3
WDR 5,https://wdr-wdr5-live.icecastssl.wdr.de/wdr/wdr5/live/mp3/128/stream.mp3
WDR Die Maus (ehem. KiRaKa),https://wdr-diemaus-live.icecastssl.wdr.de/wdr/diemaus/live/mp3/128/stream.mp3
WDR Event,https://wdr-wdr-event.icecastssl.wdr.de/wdr/wdr/event/mp3/128/stream.mp3
WDR Cosmo,https://wdr-cosmo-live.icecastssl.wdr.de/wdr/cosmo/live/mp3/128/stream.mp3

# SWR, Südwestfunk. Quelle: https://www.swr.de/unternehmen/empfang/webradio-hoeren-alle-streams-100.html
# Details zum Sender: https://de.wikipedia.org/wiki/S%C3%BCdwestfunk
# last updated 12/2023
SWR 1 BW,https://liveradio.swr.de/sw282p3/swr1bw/play.mp3
SWR 1 RP,https://liveradio.swr.de/sw282p3/swr1rp/play.mp3
SWR 2,https://liveradio.swr.de/sw282p3/swr2/play.mp3
SWR 3,https://liveradio.swr.de/sw282p3/swr3/play.mp3
SWR 4 Stuttgart,https://liveradio.swr.de/sw282p3/swr4bw/play.mp3
SWR 4 Bodensee (Friedrichshafen),https://liveradio.swr.de/sw282p3/swr4fn/play.mp3
SWR 4 Südbaden (Freiburg i. Bresigau),https://liveradio.swr.de/sw282p3/swr4fr/play.mp3
SWR 4 Franken (Heilbronn),https://liveradio.swr.de/sw282p3/swr4hn/play.mp3
SWR 4 Baden (Karlsruhe),https://liveradio.swr.de/sw282p3/swr4ka/play.mp3
SWR 4 Kaiserslautern,https://liveradio.swr.de/sw282p3/swr4kl/play.mp3
SWR 4 Koblenz,https://liveradio.swr.de/sw282p3/swr4ko/play.mp3
SWR 4 Ludwigshafen,https://liveradio.swr.de/sw282p3/swr4lu/play.mp3
SWR 4 Kurpfalz (Mannheim),https://liveradio.swr.de/sw282p3/swr4ma/play.mp3
SWR 4 Mainz,https://liveradio.swr.de/sw282p3/swr4rp/play.mp3
SWR 4 Trier,https://liveradio.swr.de/sw282p3/swr4tr/play.mp3
SWR 4 Tübingen,https://liveradio.swr.de/sw282p3/swr4tu/play.mp3
SWR 4 Schwaben (Ulm),https://liveradio.swr.de/sw282p3/swr4ul/play.mp3
SWR Aktuell,https://liveradio.swr.de/sw282p3/swraktuell/play.mp3
SWR Das Ding,https://liveradio.swr.de/sw282p3/dasding/play.mp3

# SR, Saarländischer Rundfunk. Quelle: https://www.sr.de/sr/home/der_sr/so_kommunizieren_wir/service/frequenzen/frequenzen_und_livestreams_sr_de_100.html
# Details zum Sender: https://de.wikipedia.org/wiki/Saarl%C3%A4ndischer_Rundfunk
# last updated 12/2023
SR 1,http://streaming01.sr-online.de/sr1_2.m3u
SR 1 Like-O-Mat,http://streaming02.sr-online.de/sr1like-o-mat.m3u
SR 1 Lounge,http://streaming02.sr-online.de/sr1lounge.m3u
SR 2,http://streaming01.sr-online.de/sr2_2.m3u
SR 2 Offbeat,http://streaming02.sr-online.de/sr2offbeat.m3u
SR 3,http://streaming01.sr-online.de/sr3_2.m3u
SR 3 Oldiewelt,http://streaming02.sr-online.de/sr3oldiewelt.m3u
SR 3 Schlagerwelt,http://streaming02.sr-online.de/sr3schlagerwelt.m3u
SR Antenne Saar,http://streaming01.sr-online.de/antennesaar_2.m3u
SR Unser Ding,http://streaming01.sr-online.de/unserding_2.m3u
SR Unser Ding Schwarz,http://streaming02.sr-online.de/unserding-schwarz.m3u
SR Unser Unser Ding Zukunft,http://streaming02.sr-online.de/unserding-zukunft.m3u

# RBB Radio Berlin Brandenburg. Quelle: https://www.rbb-online.de/radio/frequenzen/
# Details zum Sender: https://de.wikipedia.org/wiki/Rundfunk_Berlin-Brandenburg
# last updated 12/2023
RBB Antenne Brandenburg,http://antennebrandenburg.de/livemp3
RBB 88.8 Berlin,http://rbb888.de/livemp3
RBB Fritz,http://fritz.de/livemp3
RBB 24 Inforadio,http://inforadio.de/livemp3
RBB Kultur,http://rbbkultur.de/livemp3
RBB Radio Eins,http://radioeins.de/livemp3

# RB Radio Bremen. Quelle: https://www.bremeneins.de/info/frequenzen-106.html
# Details zum Sender: https://de.wikipedia.org/wiki/Radio_Bremen
# last updated 12/2023
RB 1,https://icecast.radiobremen.de/rb/bremeneins/live/mp3/128/stream.mp3
RB 2,https://icecast.radiobremen.de/rb/bremenzwei/live/mp3/128/stream.mp3
RB 4,https://icecast.radiobremen.de/rb/bremenvier/live/mp3/128/stream.mp3
RB Next,https://icecast.radiobremen.de/rb/bremennext/live/mp3/128/stream.mp3

# NDR, Norddeutscher Rundfunk. Quelle: https://www.ndr.de/service/livestreams101_page-1.html
# Details zum Sender: https://de.wikipedia.org/wiki/Norddeutscher_Rundfunk
# last updated 12/2023
NDR 1 Hannover,http://www.ndr.de/resources/metadaten/audio/m3u/ndr1niedersachsen.m3u
NDR 1 Braunschweig,https://www.ndr.de/resources/metadaten/audio/m3u/ndr1niedersachsen_bs.m3u
NDR 1 Lüneburg,https://www.ndr.de/resources/metadaten/audio/m3u/ndr1niedersachsen_lg.m3u
NDR 1 Oldenburg,https://www.ndr.de/resources/metadaten/audio/m3u/ndr1niedersachsen_ol.m3u
NDR 1 Osnabrück,https://www.ndr.de/resources/metadaten/audio/m3u/ndr1niedersachsen_os.m3u
NDR 1 Schwerin,https://www.ndr.de/resources/metadaten/audio/m3u/ndr1radiomv.m3u
NDR 1 Greifswald,https://www.ndr.de/resources/metadaten/audio/m3u/ndr1radiomv_hgw.m3u
NDR 1 Rostock,https://www.ndr.de/resources/metadaten/audio/m3u/ndr1radiomv_hro.m3u
NDR 1 Neubrandenburg,https://www.ndr.de/resources/metadaten/audio/m3u/ndr1radiomv_nb.m3u
NDR 90.3,http://www.ndr.de/resources/metadaten/audio/m3u/ndr903.m3u
NDR 1 Kiel,http://www.ndr.de/resources/metadaten/audio/m3u/ndr1wellenord.m3u
NDR 1 Flensburg,https://www.ndr.de/resources/metadaten/audio/m3u/ndr1wellenord_fl.m3u
NDR 1 Heide,https://www.ndr.de/resources/metadaten/audio/m3u/ndr1wellenord_hei.m3u
NDR 1 Lübeck,https://www.ndr.de/resources/metadaten/audio/m3u/ndr1wellenord_hl.m3u
NDR 1 Norderstedt,https://www.ndr.de/resources/metadaten/audio/m3u/ndr1wellenord_nor.m3u
NDR 2 Niedersachsen,http://www.ndr.de/resources/metadaten/audio/m3u/ndr2.m3u
NDR 2 Hamburg,https://www.ndr.de/resources/metadaten/audio/m3u/ndr2_hh.m3u
NDR 2 Mecklenburg-Vorpommern,https://www.ndr.de/resources/metadaten/audio/m3u/ndr2_mv.m3u
NDR 2 Schleswig-Holstein,https://www.ndr.de/resources/metadaten/audio/m3u/ndr2_sh.m3u
NDR Kultur,http://www.ndr.de/resources/metadaten/audio/m3u/ndrkultur.m3u
NDR Info Niedersachsen,http://www.ndr.de/resources/metadaten/audio/m3u/ndrinfo.m3u
NDR Info Hamburg,https://www.ndr.de/resources/metadaten/audio/m3u/ndrinfo_hh.m3u
NDR Info Mecklenburg-Vorpommern,https://www.ndr.de/resources/metadaten/audio/m3u/ndrinfo_mv.m3u
NDR Info Schleswig-Holstein,https://www.ndr.de/resources/metadaten/audio/m3u/ndrinfo_sh.m3u
NDR Info Spezial,https://www.ndr.de/resources/metadaten/audio/m3u/ndrinfo_spezial.m3u
NDR N-Joy,http://www.ndr.de/resources/metadaten/audio/m3u/n-joy.m3u
NDR Blue,http://www.ndr.de/resources/metadaten/audio/m3u/ndrblue.m3u
NDR Schlager,https://www.ndr.de/resources/metadaten/audio/m3u/ndrschlager.m3u

# HR, Hessischer Rundfunk. Quelle: https://download.hr.de/services/empfang-und-verbreitungswege/empfang-livestreams-wellen-100.pdf
# Details zum Sender: https://de.wikipedia.org/wiki/Hessischer_Rundfunk
# last updated 12/2023
HR 1 Nordhessen,https://dispatcher.rndfnk.com/hr/hr1/nordhessen/high
HR 1 Osthessen,http://dispatcher.rndfnk.com/hr/hr1/osthessen/high
HR 1 Mittelhessen,https://dispatcher.rndfnk.com/hr/hr1/mittelhessen/high
HR 1 Rhein-Main,https://dispatcher.rndfnk.com/hr/hr1/rheinmain/high
HR 1 Südhessen,https://dispatcher.rndfnk.com/hr/hr1/suedhessen/high
HR 2,https://dispatcher.rndfnk.com/hr/hr2/live/high
HR Info,https://dispatcher.rndfnk.com/hr/hrinfo/live/high
HR You,https://dispatcher.rndfnk.com/hr/youfm/live/high
HR 3 Nordhessen,https://dispatcher.rndfnk.com/hr/hr3/nordhessen/high
HR 3 Osthessen,https://dispatcher.rndfnk.com/hr/hr3/osthessen/high
HR 3 Mittelhessen,https://dispatcher.rndfnk.com/hr/hr3/mittelhessen/high
HR 3 Rhein-Main,https://dispatcher.rndfnk.com/hr/hr3/rheinmain/high
HR 3 Südhessen,https://dispatcher.rndfnk.com/hr/hr3/suedhessen/high
HR 4 Nordhessen,https://dispatcher.rndfnk.com/hr/hr4/nordhessen/high
HR 4 Osthessen,https://dispatcher.rndfnk.com/hr/hr4/osthessen/high
HR 4 Mittelhessen,https://dispatcher.rndfnk.com/hr/hr4/mittelhessen/high
HR 4 Rhein-Main,https://dispatcher.rndfnk.com/hr/hr4/rheinmain/high
HR 4 Südhessen,https://dispatcher.rndfnk.com/hr/hr4/suedhessen/high

# MDR, Mitteldeutscher Rundfunk. Quelle: https://www.mdr.de/radio/frequenzen/index.html
# Details zum Sender: https://de.wikipedia.org/wiki/Mitteldeutscher_Rundfunk
# last updated 12/2023
MDR Aktuell,http://mdr-284340-0.cast.mdr.de/mdr/284340/0/mp3/high/stream.mp3
MDR Jumpo,http://mdr-284320-0.cast.mdr.de/mdr/284320/0/mp3/high/stream.mp3
MDR Klassik,http://mdr-284350-0.cast.mdr.de/mdr/284350/0/mp3/high/stream.mp3
MDR Kultur,http://mdr-284310-0.cast.mdr.de/mdr/284310/0/mp3/high/stream.mp3
MDR Sachsen,http://mdr-284280-0.cast.mdr.de/mdr/284280/0/mp3/high/stream.mp3
MDR Sachsen (Sorbisch),http://mdr-990100-0.cast.mdr.de/mdr/990100/0/mp3/high/stream.mp3
MDR Sachsen-Anhalt,http://mdr-284290-3.cast.mdr.de/mdr/284290/3/mp3/high/stream.mp3
MDR Sachsen Schlagerwelt,http://mdr-990801-0.cast.mdr.de/mdr/990801/0/mp3/high/stream.mp3
MDR Sputnik,http://mdr-284330-0.cast.mdr.de/mdr/284330/0/mp3/high/stream.mp3
MDR Thüringen,http://mdr-284300-3.cast.mdr.de/mdr/284300/3/mp3/high/stream.mp3
MDR Tweens,http://avw.mdr.de/streams/991101-0_mp3_high.m3u

# BR, Bayerischer Rundfunk, Quelle: https://www.br.de/service/m3u-livestreams-100.html
# Details zum Sender: https://de.wikipedia.org/wiki/Bayerischer_Rundfunk
# last updated 12/2023
BR 1 Oberbayern,https://streams.br.de/bayern1obb_2.m3u
BR 1 Niederbayern/Oberpfalz,https://streams.br.de/bayern1nbopf_2.m3u
BR 1 Schwaben,https://streams.br.de/bayern1schw_2.m3u
BR 1 Franken,https://streams.br.de/bayern1fran_2.m3u
BR 1 Mainfranken,https://streams.br.de/bayern1main_2.m3u
BR 2 Nord,https://streams.br.de/bayern2nord_2.m3u
BR 2 Süd,https://streams.br.de/bayern2sued_2.m3u
BR 3,https://streams.br.de/bayern3_2.m3u
BR Klassik,https://streams.br.de/br-klassik_2.m3u
BR Klassik (HQ),https://streams.br.de/br-klassik_3.m3u
BR 24,https://streams.br.de/br24_2.m3u
BR24 Live,https://streams.br.de/br24live_2.m3u
BR Schlager,https://streams.br.de/brschlager_2.m3u
BR Heimat,https://streams.br.de/brheimat_2.m3u
BR Puls,https://streams.br.de/puls_2.m3u

# DLF Deutschlandfunk. Quelle: https://www.deutschlandradio.de/streamingdienste-100.html
# Details zum Sender: https://de.wikipedia.org/wiki/Deutschlandradio
# last updated 12/2023
DLF,https://st01.sslstream.dlf.de/dlf/01/high/aac/stream.aac?aggregator=web
DLF Kultur,https://st02.sslstream.dlf.de/dlf/02/high/aac/stream.aac?aggregator=web
DLF Nova,https://st03.sslstream.dlf.de/dlf/03/high/aac/stream.aac?aggregator=web
DLF Dokumente und Debatten,https://st04.sslstream.dlf.de/dlf/04/high/aac/stream.aac?aggregator=web

# RDL Radio Dreyeckland. Quelle: https://rdl.de/beitrag/rdl-geschichte
# Details zum Sender: https://de.wikipedia.org/wiki/Radio_Dreyeckland_(Freiburg_im_Breisgau)
RDL Sender Freiburg,https://www.rdl.de:8000/rdl.m3u
# auch: https://stream.rdl.de/rdl
